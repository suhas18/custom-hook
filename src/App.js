import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import useLogger from './useLogger';

function App() {
  const [text, setText] = useState(''); 
  useLogger(text)
  return (
   <>
   <input type="text" className="textName" value={text} placeholder="Type Anything Here..." onChange={e => setText(e.target.value)}></input>
   <h2 className='htag'>This is an example for custom hooks.
   <br/>
    I created a custom hook that logs the text which you type 
    in the input field.<br/>
    The Output is displayed on console log.
   </h2>
   </>
  );
}

export default App;
