import { useEffect } from "react";

export default function useLogger(text){
   useEffect(()=>{
    console.log(text);
   },[text])
}
